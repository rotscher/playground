'use strict';

/**
 * @ngdoc function
 * @name sceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sceApp
 */
angular.module('sceApp')
  .controller('MainCtrl', ['$scope', '$sce', function ($scope, $sce) {

    $sce.isEnabled(false);
    $scope.foobar = 'hallo <b>hdddd</b>';

    $scope.html = '<script>alert(1)</script><h1 onclick="alert(1)">Hello World!</h1>';
    $scope.sceEnabled = $sce.isEnabled();
  }]);
